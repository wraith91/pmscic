<?php

namespace App;

use App\Instrument;
use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
	/**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'locations';

  /**
	  * The attributes that are mass assignable.
	  *
	  * @var array
	  */
  protected $fillable = ['code', 'name'];

  public $timestamps = false;

  public function instruments()
  {
      return $this->belongsTo('Instrument');
  }
}
