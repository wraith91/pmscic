<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Instrument extends Model
{
	/**
   * The database table used by the model.
   *
   * @var string
   */
  	protected $table = 'instruments';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  	protected $fillable = ['location_id', 'instrument_id', 'description', 'application', 'trade_name', 'specification']; 

    /**
     * Get the department associated with the instrument.
     */
    public function location()
    {
        return $this->hasOne('App\Location', 'id', 'location_id');
    }

    public function testDates()
    {
        return $this->hasMany(InstrumentTestDate::class);
    }

    public function tests()
    {
        return $this->hasMany(InstrumentTest::class);
    }

    public function scopeSearch($query, $search)
    {
        return $query->where('instrument_id', $search);
    }
}
