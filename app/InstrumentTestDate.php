<?php

namespace App;

use Carbon\Carbon;

use Illuminate\Database\Eloquent\Model;

class InstrumentTestDate extends Model
{
	 /**
    * The database table used by the model.
    *
    * @var string
    */
   protected $table = 'instrument_test_dates';

   /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
   protected $fillable = ['calibrated_at', 'recalled_at'];
	  /**
	   * The attributes that should be mutated to dates.
	   *
	   * @var array
	   */
   protected $dates = ['calibrated_at', 'recalled_at'];

   public function setCalibratedAtAttribute($date)
   {
      $this->attributes['calibrated_at'] = Carbon::parse($date);
   }

   public function setRecalledAtAttribute($date)
   {
      $this->attributes['recalled_at'] = Carbon::parse($date);
   }

   public function instrument()
   {
   		return $this->belongsTo(Instrument::class, 'id', 'instrument_id');
   }
}
