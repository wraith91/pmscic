<?php

namespace App\Http\Controllers;

use App\Location;
use App\Instrument;
use App\InstrumentTest;
use App\InstrumentTestDate;
use App\Http\Requests\InstrumentRequest;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryExceptions;
use PDOException;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class InstrumentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request = null)
    {
        if ($request->get('search'))
        {
            $id = $request->get('search');
            try
            {
                $instrument = Instrument::search($id)->firstOrFail();

                $testDate = InstrumentTestDate::where('instrument_id', $instrument->id)->orderBy('created_at', 'desc')->first();
            }
            catch (ModelNotFoundException $e)
            {
                return redirect()->back()->withErrors('No instrument was found...');
            }
        }

        return view('app.instrument.index', compact('instrument', 'testDate'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try 
        {
            $location = Location::lists('name', 'id');
        }
        catch(ModelNotFoundException $e)
        {
            return redirect()->dashboard();
        }
        catch (PDOException $e)
        {
            return redirect()->back();
        }

        return view('app.instrument.create', compact('location'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try 
        {
            $instrument = Instrument::with('location')->findOrFail($id);

            $location = Location::lists('name', 'id');

            $testDates = InstrumentTestDate::where('instrument_id', $id)->orderBy('created_at', 'desc')->first();
        }
        catch(ModelNotFoundException $e)
        {
            return redirect()->dashboard();
        }
 
        return view('app.instrument.edit', compact('instrument', 'location', 'testDates'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(InstrumentRequest $request)
    {
        try 
        {
            $instrument = Instrument::create($request->except('calibrated_at', 'recalled_at'));
        } 
        catch (QueryException $e) 
        {
            return redirect()->back()->withInput()->withErrors('Instrument ID exist in the database');
        }

        $testDates = new InstrumentTestDate([
            'calibrated_at' => $request->calibrated_at,
            'recalled_at' => $request->recalled_at,
        ]);

        $instrument->testDates()->save($testDates);

        return redirect('instrument');  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $instrument = Instrument::find($id);

        $testDate = InstrumentTestDate::where('instrument_id', $id)->orderBy('created_at', 'desc')->first();

        return view('app.instrument.show', compact('instrument', 'testDate'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(InstrumentRequest $request, $id)
    {
        try
        {
            $instrument = Instrument::findOrFail($id);

            $update = $request->except('_method', '_token');
            
            if($instrument->fill($update)->save())
            {
                session()->flash('flash_message', 'Instrument detail has been updated.');
            }
            
        }
        catch(ModelNotFoundException $e)
        {
            return redirect()->back()->withErrors('Instrument ID not found.');
        }
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $instrument = Instrument::findOrFail($id);

        if($instrument->delete())
        {
            session()->flash('flash_message', 'Instrument with the ID of ' . $instrument->instrument_id  . ' has been deleted.');
        }
        else 
        {
            redirect()->back()->withErrors('No instrument is deleted.');
        }

        return redirect()->back();
    }

    public function history($id)
    {
        $instrument = Instrument::find($id);

        $dates = InstrumentTestDate::where('instrument_id', $id)->orderBy('created_at', 'desc')->get();

        $i = 1; 
        
        return view('app.instrument.history', compact('dates', 'instrument', 'i'));
    }

    public function lists()
    {
        $instruments = Instrument::all();

        $i = 1;

        return view('app.instrument.list', compact('instruments', 'i'));
    }

}