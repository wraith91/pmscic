<?php

namespace App\Http\Controllers;

use App\User;
use App\Instrument;
use App\Calibration;
use App\InstrumentTestDate;
use App\InstrumentTest;
use App\Http\Requests\InstrumentTestRequest;
use App\Http\Requests\CalibrationRequest;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class CalibrationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request = null)
    {
        if ($request->get('search'))
        {
            $id = $request->get('search');
            try
            {
                $instrument = Instrument::search($id)->firstOrFail();

                $testDate = InstrumentTestDate::where('instrument_id', $instrument->id)->orderBy('created_at', 'desc')->first();
            }
            catch (ModelNotFoundException $e)
            {
                return redirect()->back()->withErrors('No instrument was found...');
            }
        }

        return view('app.calibration.index', compact('instrument', 'testDate'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $instrument = Instrument::with([
                        'testDates' => function ($query)
                        {
                            return $query->orderBy('created_at', 'desc')->first();
                        }, 
                        'tests' => function ($query)
                        {
                            return $query->orderBy('created_at', 'desc')->take(5)->latest();
                        }, 'location'])->findOrFail($id);

        return view('app.calibration.create', compact('instrument'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CalibrationRequest $request)
    {
        $calibration = new Calibration($request->except('_token'));

        $user = User::find(auth()->user()->id);

        if ($user->calibrate()->save($calibration))
        {
            session()->flash('flash_message', 'Calibration details stored!');
        }

        return redirect('calibration/' . $request->instrument_id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $instrument = Instrument::with([
                        'testDates' => function ($query)
                        {
                            return $query->orderBy('created_at', 'desc')->first();
                        }, 
                        'tests' => function ($query)
                        {
                            return $query->orderBy('created_at', 'desc')->take(5)->latest();
                        }, 'location'])->findOrFail($id);

        $calibration = Calibration::where('instrument_id', $instrument->id)->orderBy('created_at', 'desc')->take(1)->get();

        return view('app.calibration.show', compact('instrument', 'calibration'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $instrument = Instrument::with(['tests' => function ($query){
                        return $query->orderBy('created_at', 'desc')->take(5);
        }])->findOrFail($id);

        return view('app.calibration.edit', compact('instrument'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $instrumentTest = InstrumentTest::findMany($request->test_id);
            
        $data = $request->except('_method', '_token', 'instrument_id', 'test_id');

        $tests = [];

        for ($i=0; $i < count($data); $i++) {
            $tests[] = [
                'scale'             => $data['scale'][$i],
                'input'             => $data['input'][$i],
                'output'            => $data['output'][$i],
                'after_calibration' => $data['after_calibration'][$i],
                'error'             => $data['error'][$i]
            ];

           $saved = $instrumentTest[$i]->fill($tests[$i])->save();
        }

        if ($saved)
        {
            session()->flash('flash_message', 'Instrument Test has been updated!');
        }

        return redirect()->intended('calibration/' . $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
