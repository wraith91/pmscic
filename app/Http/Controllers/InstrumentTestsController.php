<?php

namespace App\Http\Controllers;

use App\Instrument;
use App\InstrumentTest;
use App\Http\Requests\InstrumentTestRequest;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class InstrumentTestsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $instrument = Instrument::findOrFail($id);

        return view('app.test.create', compact('instrument'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(InstrumentTestRequest $request)
    {
        $id = $request->instrument_id;

        $instrument = Instrument::findOrFail($id);

        $data = $request->except('_token', 'instrument_id');

        $tests = [];

        for ($i=0; $i<count($data); $i++) {
            $tests[] = new InstrumentTest([
                'scale'             => $data['scale'][$i],
                'input'             => $data['input'][$i],
                'output'            => $data['output'][$i],
                'after_calibration' => $data['after_calibration'][$i],
                'error'             => $data['error'][$i],
                'test_date'         => date('Y-m-d')
            ]);
        }

        if ($instrument->tests()->saveMany($tests))
        {
            session()->flash('flash_message', 'Instrument has been calibrated');
        }

        return redirect(route('calibration.create') . '/' . $instrument->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return 'Hello';
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $instrument = Instrument::with(['tests' => function ($query){
                        return $query->orderBy('created_at', 'desc')->take(5);
        }])->findOrFail($id);

        // return $instrument;
        return view('app.test.edit', compact('instrument'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $instrumentTest = InstrumentTest::findMany($request->test_id);
            
        $data = $request->except('_method', '_token', 'instrument_id', 'test_id');

        $tests = [];

        for ($i=0; $i < count($data); $i++) {
            $tests[] = [
                'scale'             => $data['scale'][$i],
                'input'             => $data['input'][$i],
                'output'            => $data['output'][$i],
                'after_calibration' => $data['after_calibration'][$i],
                'error'             => $data['error'][$i]
            ];

           $saved = $instrumentTest[$i]->fill($tests[$i])->save();
        }

        if ($saved)
        {
            session()->flash('flash_message', 'Instrument Test has been updated!');
        }

        return redirect()->intended('calibration');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
