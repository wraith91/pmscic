<?php

namespace App\Http\Controllers;

use App\Certificate;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class CertificateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $certifications = Certificate::all();

        $i = 1;

        return view('app.system.certificate.index', compact('certifications', 'i'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('app.system.certificate.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $certificate = Certificate::create($request->except('_token'));

        if ($certificate) 
        {
            session()->flash('flash_message', 'Added new certification!');
        }

        return redirect('system/certificate');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $certificate = Certificate::find($id);

        return view('app.system.certificate.edit', compact('certificate'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, ['cert_id' => 'required', 'type' => 'required']);

        $certificate = Certificate::find($id);

        if ($certificate->fill($request->only('cert_id', 'type'))->save())
        {
            session()->flash('flash_message', 'Certificate has been modified!');
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cert = Certificate::find($id);

        if ($cert->delete())
        {
            session()->flash('flash_message', 'Cert has been successfully deleted!');
        }

        return redirect()->back();

    }
}
