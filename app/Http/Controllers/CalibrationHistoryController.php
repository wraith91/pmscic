<?php

namespace App\Http\Controllers;

use App\Instrument;
use App\Calibration;
use App\InstrumentTest;
use App\InstrumentTestDate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class CalibrationHistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request = null)
    {
        if ($request->get('search') && $request->get('date'))
        {
            $id = $request->get('search');
            $date = $request->get('date');
            try
            {
                $instrument = Instrument::search($id)->firstOrFail();

                $calibration = Calibration::where('instrument_id', $instrument->id)->where('calibrated_at', $date)->get();
            }
            catch (ModelNotFoundException $e)
            {
                return redirect()->back()->withErrors('No calibration was found...');
            }
        }

        $i = 1;

        return view('app.calibration.history.index', compact('calibration', 'instrument', 'i'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $instrument_id = Input::only('instrument_id');
        
        $date = Input::only('calibrated_at');

        $dates = InstrumentTestDate::where('instrument_id', $instrument_id)->get();

        $tests = InstrumentTest::where('test_date', $date)->where('instrument_id', $instrument_id)->orderBy('created_at', 'desc')->take(5)->get();

        $calibration = Calibration::with('instrument.location', 'instrument')->where('calibrated_at', $date)->where('instrument_id', $instrument_id)->get();

        return view('app.calibration.history.show', compact('calibration', 'tests', 'dates'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
