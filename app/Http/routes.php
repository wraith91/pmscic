<?php

Route::get('/', ['as' => 'login', 'middleware' => 'guest', function () {
    return view('auth.login');
}]);

// Authentication routes...
Route::get('auth/login', ['uses' => 'Auth\AuthController@getLogin', 'middleware' => 'guest']);
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

Route::group(['middleware' => ['auth']], function()
{
	Route::get('dashboard', ['as' => 'dashboard', function () {
    return view('app.dashboard');
	}]);

	// Registration routes...
	Route::get('account/register', 'RegistrationController@create');
	Route::post('account/register', 'RegistrationController@store');

	Route::get('account/settings', 'AccountController@index');
	Route::get('account/settings/{id}/edit', 'AccountController@edit');
	Route::patch('account/settings/{id}', 'AccountController@update');

	// Instrument routes...
	// Route::get('instrument/lists', 'InstrumentController@lists');
	Route::get('instrument/lists', 'InstrumentController@lists');
	Route::get('instrument/test/{id}', 'InstrumentTestsController@edit');
	Route::post('instrument/{id}', 'InstrumentTestDatesController@store');
	Route::get('instrument/{id}/history', 'InstrumentController@history');
	Route::resource('instrument', 'InstrumentController');

	Route::group(['middleware' => 'customer'], function()
	{
		Route::get('calibration/create/{id}', ['as' => 'calibration.create', 'uses' => 'CalibrationController@create']);
		Route::get('calibration/history/', 'CalibrationHistoryController@index');
		Route::get('calibration/history/{id}', 'CalibrationHistoryController@show');
		Route::resource('calibration', 'CalibrationController');

		Route::get('calibration/test/create/{id}', ['as' => 'test.create', 'uses' => 'InstrumentTestsController@create']);
		Route::post('calibration/test', 'InstrumentTestsController@store');
		Route::patch('calibration/test/{id}', 'InstrumentTestsController@update');
		Route::get('calibration/test/{id}/edit', 'InstrumentTestsController@edit');

		Route::get('system', function(){
			return view('app.system.index');
		});

		Route::get('system/location', 'LocationController@index');
		Route::get('system/location/create', 'LocationController@create');
		Route::post('system/location', 'LocationController@store');
		Route::delete('system/location/{id}', 'LocationController@destroy');

		Route::resource('system/user', 'UserController');	
		
		Route::resource('system/role', 'RolePermissionController');	

		Route::resource('system/certificate', 'CertificateController');
	});


});