<?php

namespace App\Http\Requests;

use Auth;
use App\Http\Requests\Request;

class InstrumentRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'instrument_id' => 'required',
            'description'   => 'required',
            'application'   => 'required',
            'trade_name'    => 'required',
            'specification' => 'required',
        ];
    }
}
