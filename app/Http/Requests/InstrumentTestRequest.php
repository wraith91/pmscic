<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class InstrumentTestRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'scale' => 'array|required'
        ];

        for ($i=0; count($this->request->get('scale')) <= 4 ; $i++) { 
            $rules['scale'.$i] = 'required';
        }

        return $rules;
    }
}
