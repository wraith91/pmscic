<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CalibrationRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'customer' => 'required',
            'address' => 'required',
            'input' => 'required',
            'output' => 'required',
            'model' => 'required',
            'serial_no' => 'required',
            'ref_id' => 'required',
            'done_by' => 'required',
            'approve_by' => 'required'
        ];
    }
}
