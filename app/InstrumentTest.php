<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class InstrumentTest extends Model
{
		/**
	   * The database table used by the model.
	   *
	   * @var string
	   */
	  protected $table = 'instrument_tests';

	  protected $fillable = ['scale', 'input', 'output', 'after_calibration', 'error', 'test_date'];

	  protected $dates = ['test_date'];

	  public function setTestDateAttribute($date)
	  {
	  		$this->attributes['test_date'] = Carbon::parse($date);
	  }

		public function instrument()
		{
				return $this->belongsTo(Instrument::class, 'id', 'instrument_id');
		}
}
