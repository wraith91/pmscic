<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Calibration extends Model
{
    /**
   * The database table used by the model.
   *
   * @var string
   */
  	protected $table = 'calibrations';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  	protected $fillable = ['user_id', 'instrument_id', 'customer', 'address', 'input', 'output', 'model', 'tag', 'serial_no', 'remarks', 'ref_id', 'calibrated_at', 'done_by', 'approve_by', 'other_specs']; 

    protected $dates = ['calibrated_at'];

    /**
     * Get the department associated with the instrument.
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'id', 'user_id');
    }

    public function instrument()
    {
    		return $this->hasOne('App\Instrument', 'id', 'instrument_id');
    }
}
