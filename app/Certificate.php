<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Certificate extends Model
{
	/**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'certifications';

  /**
	  * The attributes that are mass assignable.
	  *
	  * @var array
	  */
  protected $fillable = ['cert_id', 'type'];
}
