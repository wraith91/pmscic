<?php

use App\Role;
use App\Permission;
use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->delete();
		DB::table('permissions')->delete();

        Role::create([
        	'name' => 'admin',
        	'description' => 'The administrator of the site.'
        ]);

        Role::create([
        	'name' => 'employee',
        	'description' => 'The editor of the site records.'
        ]);

        Role::create([
        	'name' => 'customer',
        	'description' => 'An eligible customer who can view calibrated outputs'
        ]);

        Permission::create([
            'name' => 'view_instruments',
            'description' => 'Can view instruments'
        ]);

        Permission::create([
            'name' => 'add_instruments',
            'description' => 'Can add instruments'
        ]);

        Permission::create([
            'name' => 'delete_instruments',
            'description' => 'Can delete instruments'
        ]);

        Permission::create([
            'name' => 'edit_instruments',
            'description' => 'Can edit instruments'
        ]);

        Permission::create([
            'name' => 'view_calibrations',
            'description' => 'Can view calibrations'
        ]);

        Permission::create([
            'name' => 'add_calibrations',
            'description' => 'Can add calibrations'
        ]);

        Permission::create([
            'name' => 'edit_calibrations',
            'description' => 'Can edit calibrations'
        ]);

        Permission::create([
            'name' => 'delete_calibrations',
            'description' => 'Can delete calibrations'
        ]);

        Permission::create([
            'name' => 'edit_system_settings',
            'description' => 'Can edit system settings'
        ]);

        Permission::create([
            'name' => 'view_dashboard',
            'description' => 'Can view dashboard'
        ]);
    }
}
