<?php

use App\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();

        $user = User::create([
        	'first_name' => 'Karen',
        	'last_name' => 'Starfruit',
        	'email' => 'karen@gmail.com',
        	'password' => 'qweasd',
        ]);

        $user->assignRole('admin');
    }
}
