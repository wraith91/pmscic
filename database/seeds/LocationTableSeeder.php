<?php

use App\Location;
use Illuminate\Database\Seeder;

class LocationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('locations')->delete();
    
    		Location::create([
    			'code' => '07',
    			'name' => 'Cpas - 07'
    		]);

    		Location::create([
    			'code' => '08',
    			'name' => 'Cpas - 08'
    		]);

    		Location::create([
    			'code' => '66',
    			'name' => 'Cpas - 66'
    		]);

    		Location::create([
    			'code' => '08',
    			'name' => 'Ballestra'
    		]);

    		Location::create([
    			'code' => '100',
    			'name' => 'Multimix'
    		]);

    		Location::create([
    			'code' => '78',
    			'name' => 'Ntr Powders - 78'
    		]);

    		Location::create([
    			'code' => '81',
    			'name' => 'Ntr Powders - 81'
    		]);

    		Location::create([
    			'code' => '84',
    			'name' => 'Ntr Powders - 84'
    		]);

    		Location::create([
    			'code' => 'B0',
    			'name' => 'Selecta'
    		]);

    		Location::create([
    			'code' => '01',
    			'name' => 'Zolberg'
    		]);
    }
}