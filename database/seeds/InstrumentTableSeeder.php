<?php

use App\Instrument;
use App\InstrumentTestDate;
use Carbon\Carbon;

use Illuminate\Database\Seeder;

class InstrumentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('instruments')->delete();
        DB::table('instrument_test_dates')->delete();

        $instrument = Instrument::create([
        	'location_id' => 5,
        	'instrument_id' => 'I0823',
        	'description' => 'Temp.Gauge',
        	'trade_name' => 'Spriano Milano',
        	'specification' => 'Range: 0 to 160 deg. C; 6" face dia.',
        	'application' => 'Ex Silica Bed 1 Exit Air Temp.'
        ]);

        InstrumentTestDate::create([
        	'instrument_id' => $instrument->id,
        	'calibrated_at' => Carbon::now(),
        	'recalled_at' => Carbon::now()->addYear()
        ]);

    }
}
