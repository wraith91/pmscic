<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstrumentTestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('instrument_tests', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('instrument_id')->unsigned();
            $table->string('scale');
            $table->string('input');
            $table->string('output');
            $table->string('after_calibration');
            $table->string('error');       
            $table->timestamp('test_date');       
            $table->timestamps();

            $table->foreign('instrument_id')
                  ->references('id')
                  ->on('instruments')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('instrument_tests');
    }
}
