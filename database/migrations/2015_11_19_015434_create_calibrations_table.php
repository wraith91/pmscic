<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCalibrationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calibrations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('instrument_id')->unsigned();
            $table->string('customer');
            $table->string('address');
            $table->string('input')->nullable();
            $table->string('output')->nullable();
            $table->string('model')->nullable();
            $table->string('tag')->nullable();
            $table->string('serial_no')->nullable();
            $table->string('other_specs')->nullable();
            $table->string('remarks')->nullable();
            $table->string('ref_id')->nullable();
            $table->string('done_by')->nullable();
            $table->string('approve_by')->nullable();
            $table->timestamp('calibrated_at');


            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->foreign('instrument_id')
                ->references('id')
                ->on('instruments')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('calibrations');
    }
}
