@extends('layouts.master')

@section('title')
	Calibration
@stop

@section('header')
	@include ('layouts.secondary_menu', 
	[
		'title' => 'Calibration', 
		'secondary_menu' => 
			[
				'History' => 'calibration/history'
			],
	])
@stop

@section('content')
	<div class="search-content content-block">
		  {!! Form::open(['method' => 'GET' , 'url' => 'calibration' ,'class' => 'navbar-form']) !!}
		  	<div class="form-group">
		  		{!! Form::label('search', 'Search Instrument ID:') !!}
		  	  {!! Form::text('search', null, ['class' => 'form-control', 'required']) !!}
		  	</div>
		  		{!! Form::submit('Search', ['class' => 'btn btn-default']) !!}
		  {!! Form::close() !!}
	</div>
	<div class="inner-content content-block">
		<div class="panel panel-default">
	  <!-- Default panel contents -->
		  <div class="panel-heading">Instrument List</div>

		  <!-- Table -->
			<table class="table table-hover">
	      <thead>
	        <tr>
	          <th>#</th>
	          <th>ID</th>
	          <th>Description</th>
	          <th>Calibration</th>
	          <th>Recall</th>
	          <th>Action</th>
	        </tr>
	      </thead>
	      <tbody>
				  @unless (empty($instrument))
						<tr>
		          <th scope="row">1</th>
		          <td><a href="{{ 'calibration/create/' . $instrument->id }}">{{ $instrument->instrument_id }}</a></td>
		          <td>{{ $instrument->description }}</td>
		          <td>{{ $testDate->calibrated_at->format('Y-m-d') }}</td>
		          <td>{{ $testDate->recalled_at->format('Y-m-d') }}</td>
		          <td>
								<div class="form-inline">
								  <a href="{{ 'calibration/test/create/' . $instrument->id }}" class="btn btn-default">Calibrate</a>
									<a href="{{ 'calibration/test/' . $instrument->id . '/edit' }}" class="btn btn-default">Edit Calibration</a>
									<a href="{{ 'calibration/' . $instrument->id  }}" class="btn btn-default">View Calibration</a>
								</div>
		          </td>
		        </tr>
				  @endif
	      </tbody>
	    </table>
		</div>
	</div>
@stop