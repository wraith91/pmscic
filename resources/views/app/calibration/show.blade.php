@extends('layouts.master')

@section('title')
	Calibration
@stop

@section('header')
	@include ('layouts.secondary_menu', 
	[
		'title' => 'Calibration', 
		'secondary_menu' => 
			[
				'Back' => 'calibration'
			],
	])
@stop


@section('content')
	<div class="inner-content content-block">
	  	<div class="file-title">
	  		<h1>Power ReadJj, INC.</h1>
	  		<h3>Electrical Insturmentation &amp Controls-Engineering</h3>
	  	</div>
		  <table class="preview-table">
		  	<tbody>
		  		@foreach ($calibration as $render)
		  		@foreach ($instrument->testDates as $testDate)
		  		<tr>
		  			<td><strong>Customer</strong></td>
		  			<td>:</td>
		  			<td><p>{{ $render->customer }}</p></td>
		  			<td><strong>Calibration Date</strong></td>
		  			<td>:</td>
		  			<td><p>{{ $testDate->calibrated_at->format('F, d, Y') }}</p></td>
		  		</tr>
		  		<tr>
		  			<td><strong>Address</strong></td>
		  			<td>:</td>
		  			<td><p>{{ $render->address }}</p></td>
		  			<td><strong>Recall Date</strong></td>
		  			<td>:</td>
		  			<td><p>{{ $testDate->recalled_at->format('F, d, Y') }}</p></td>
		  		</tr>
		  		@endforeach
		  		<tr>
		  			<td colspan="3"></td>
		  			<td><strong>Location</strong></td>
		  			<td>:</td>
		  			<td><p>{{ $instrument->location->name }}</p></td>
		  		</tr>
		  		<tr>
		  			<td class="title-block" colspan="6">Instrument Description</td>
		  		</tr>
		  		<tr>
		  			<td><strong>I.D #</strong></td>
		  			<td>:</td>
		  			<td><p>{{ $instrument->instrument_id }}</p></td>
		  			<td><strong>Application</strong></td>
		  			<td>:</td>
		  			<td><p>{{ $instrument->application }}</p></td>
		  		</tr>
		  		<tr>
		  			<td><strong>Function</strong></td>
		  			<td>:</td>
		  			<td><p>{{ $instrument->description }}</p></td>
		  			<td><strong>Input</strong></td>
		  			<td>:</td>
		  			<td><p>{{ $render->input }}</p></td>
		  		</tr>
		  		<tr>
		  			<td><strong>Brand</strong></td>
		  			<td>:</td>
		  			<td><p>{{ $instrument->trade_name }}</p></td>
		  			<td><strong>Output</strong></td>
		  			<td>:</td>
		  			<td><p>{{ $render->output }}</p></td>
		  		</tr>
		  		<tr>
		  			<td><strong>Model/Type</strong></td>
		  			<td>:</td>
		  			<td><p>{{ $render->model }}</p></td>
		  			<td><strong>Tag</strong></td>
		  			<td>:</td>
		  			<td><p>{{ $render->tag }}</p></td>
		  		</tr>
		  		<tr>
		  			<td><strong>Serial No.</strong></td>
		  			<td>:</td>
		  			<td><p>{{ $render->serial_no }}</p></td>
		  			<td><strong>Ref I.D</strong></td>
		  			<td>:</td>
		  			<td><p>{{ $render->ref_id }}</p></td>
		  		</tr>
		  		<tr>
		  			<td><strong>Other Specs</strong></td>
		  			<td>:</td>
		  			<td><p>{{ $render->other_specs }}</p></td>
		  			<td colspan="3"></td>
		  		</tr>
			  		<tr>
			  			<td class="title-block" colspan="6">Calibration Data</td>
			  		</tr>
		  	</tbody>
		  </table>
		  <table class="calibration-table">
		  	<thead>
		  		<tr>
		  			<th>Scale</th>
		  			<th>Input</th>
		  			<th>Output</th>
		  			<th>Calibrated Output</th>
		  			<th>Error</th>
		  		</tr>
		  	</thead>
		  	<tbody>
		  		@foreach ($instrument->tests as $tests)
						<tr>
		      		<td>{{ $tests->scale }}</td>
		      		<td>{{ $tests->input }}</td>
		      		<td>{{ $tests->output }}</td>
		      		<td>{{ $tests->after_calibration }}</td>
		      		<td>{{ $tests->error }}</td>
		      	</tr>
	      	@endforeach
		  	</tbody>
		  </table>
		  <table class="preview-table">
		  		<tr>
		  			<td colspan="6"><strong>Description of Work Done:</strong></td>
		  		</tr>
		  		<tr>
		  			<td colspan="6"><p>{{ $render->remarks }}</p></td>
		  		</tr>
		  		<tr>
		  			<td><strong>Done By</strong></td>
		  			<td>:</td>
		  			<td><p>{{ $render->done_by }}</p></td>
		  			<td colspan="3"></td>
		  		</tr>
		  		<tr>
		  			<td><strong>Approve By</strong></td>
		  			<td>:</td>
		  			<td><p>{{ $render->approve_by }}</p></td>
		  			<td colspan="3"></td>
		  		</tr>
		  </table>
		  @endforeach
	</div>
@stop