@extends('layouts.master')

@section('title')
	Calibration
@stop

@section('header')
	@include ('layouts.secondary_menu', 
	[
		'title' => 'Calibration', 
		'secondary_menu' => 
			[
				'Back' => 'calibration'
			],
	])
@stop


@section('content')
	<div class="inner-content content-block">
		  <table class="preview-table">
		    {!! Form::open(['url' => 'calibration']) !!}
		    {!! Form::hidden('instrument_id', $instrument->id) !!}
		    {!! Form::hidden('calibrated_at', date('Y-m-d')) !!}
		  	<h1>Calibration Preview</h1>
		  	<tbody>
		  		@foreach ($instrument->testDates as $testDate)
		  		<tr>
		  			<td><strong>Customer</strong></td>
		  			<td>:</td>
		  			<td><p>{!! Form::text('customer', null, ['class' => 'form-control']) !!}</p></td>
		  			<td><strong>Calibration Date</strong></td>
		  			<td>:</td>
		  			<td><p>{{ $testDate->calibrated_at->format('F, d, Y') }}</p></td>
		  		</tr>
		  		<tr>
		  			<td><strong>Address</strong></td>
		  			<td>:</td>
		  			<td><p>{!! Form::text('address', null, ['class' => 'form-control']) !!}</p></td>
		  			<td><strong>Recall Date</strong></td>
		  			<td>:</td>
		  			<td><p>{{ $testDate->recalled_at->format('F, d, Y') }}</p></td>
		  		</tr>
		  		@endforeach
		  		<tr>
		  			<td colspan="3"></td>
		  			<td><strong>Location</strong></td>
		  			<td>:</td>
		  			<td><p>{{ $instrument->location->name }}</p></td>
		  		</tr>
		  		<tr>
		  			<td class="title-block" colspan="6">Instrument Description</td>
		  		</tr>
		  		<tr>
		  			<td><strong>I.D #</strong></td>
		  			<td>:</td>
		  			<td><p>{{ $instrument->instrument_id }}</p></td>
		  			<td><strong>Application</strong></td>
		  			<td>:</td>
		  			<td><p>{{ $instrument->application }}</p></td>
		  		</tr>
		  		<tr>
		  			<td><strong>Function</strong></td>
		  			<td>:</td>
		  			<td><p>{{ $instrument->description }}</p></td>
		  			<td><strong>Input</strong></td>
		  			<td>:</td>
		  			<td><p>{!! Form::text('input', null, ['class' => 'form-control']) !!}</p></td>
		  		</tr>
		  		<tr>
		  			<td><strong>Brand</strong></td>
		  			<td>:</td>
		  			<td><p>{{ $instrument->trade_name }}</p></td>
		  			<td><strong>Output</strong></td>
		  			<td>:</td>
		  			<td><p>{!! Form::text('output', null, ['class' => 'form-control']) !!}</p></td>
		  		</tr>
		  		<tr>
		  			<td><strong>Model/Type</strong></td>
		  			<td>:</td>
		  			<td><p>{!! Form::text('model', null, ['class' => 'form-control']) !!}</p></td>
		  			<td><strong>Tag</strong></td>
		  			<td>:</td>
		  			<td><p>{!! Form::text('tag', null, ['class' => 'form-control']) !!}</p></td>
		  		</tr>
		  		<tr>
		  			<td><strong>Serial No.</strong></td>
		  			<td>:</td>
		  			<td><p>{!! Form::text('serial_no', null, ['class' => 'form-control']) !!}</p></td>
		  			<td><strong>Ref I.D</strong></td>
		  			<td>:</td>
		  			<td><p>{!! Form::text('ref_id', null, ['class' => 'form-control']) !!}</p></td>
		  		</tr>
		  		<tr>
		  			<td><strong>Other Specs</strong></td>
		  			<td>:</td>
		  			<td><p>{!! Form::text('other_specs', null, ['class' => 'form-control']) !!}</p></td>
		  			<td colspan="3"></td>
		  		</tr>
			  		<tr>
			  			<td class="title-block" colspan="6">Calibration Data</td>
			  		</tr>
		  	</tbody>
		  </table>
		  <table class="calibration-table">
		  	<thead>
		  		<tr>
		  			<th>Scale</th>
		  			<th>Input</th>
		  			<th>Output</th>
		  			<th>After Calibration</th>
		  			<th>Error</th>
		  		</tr>
		  	</thead>
		  	<tbody>
		  		@foreach ($instrument->tests as $tests)
						<tr>
		      		<td>{{ $tests->scale }}</td>
		      		<td>{{ $tests->input }}</td>
		      		<td>{{ $tests->output }}</td>
		      		<td>{{ $tests->after_calibration }}</td>
		      		<td>{{ $tests->error }}</td>
		      	</tr>
	      	@endforeach
		  	</tbody>
		  </table>
		  <table class="preview-table">
		  		<tr>
		  			<td><strong>Remarks</strong></td>
		  			<td>:</td>
		  			<td colspan="4"><p>{!! Form::textarea('remarks', null, ['class' => 'form-control']) !!}</p></td>
		  		</tr>
		  		<tr>
		  			<td><strong>Done By</strong></td>
		  			<td>:</td>
		  			<td><p>{!! Form::text('done_by', null, ['class' => 'form-control']) !!}</p></td>
		  			<td colspan="3"></td>
		  		</tr>
		  		<tr>
		  			<td><strong>Approve By</strong></td>
		  			<td>:</td>
		  			<td><p>{!! Form::text('approve_by', null, ['class' => 'form-control']) !!}</p></td>
		  			<td colspan="3"></td>
		  		</tr>
		  		<tr>
		  			<td>{!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}</td>
		  			<td colspan="5"></td>
		  		</tr>
		  </table>

    	{!! Form::close() !!}
	</div>
@stop

