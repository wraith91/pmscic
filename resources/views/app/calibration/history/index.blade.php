@extends('layouts.master')

@section('title')
	Calibration
@stop

@section('header')
	@include ('layouts.secondary_menu', 
	[
		'title' => 'Calibration', 
		'secondary_menu' => 
			[
				'Back' => 'calibration'
			],
	])
@stop

@section('content')
	<div class="search-content content-block">
		  {!! Form::open(['method' => 'GET' , 'url' => 'calibration/history' ,'class' => 'navbar-form']) !!}
		  	<div class="form-group">
		  		{!! Form::label('search', 'Search Instrument ID:') !!}
		  	  {!! Form::text('search', null, ['class' => 'form-control', 'required']) !!}
		  	</div>
		  	<div class="form-group">
		  		{!! Form::label('date', 'Date:') !!}
		  	  {!! Form::input('date', 'date', date('Y-m-d'), ['class' => 'form-control', 'required']) !!}
		  	</div>
		  		{!! Form::submit('Search', ['class' => 'btn btn-default']) !!}
		  {!! Form::close() !!}
	</div>
	<div class="inner-content content-block">
		<div class="panel panel-default">
	  <!-- Default panel contents -->
		  <div class="panel-heading">Calibration List</div>

		  <!-- Table -->
			<table class="table table-hover">
	      <thead>
	        <tr>
	          <th>#</th>
	          <th>Instrument ID</th>
	          <th>Customer</th>
	          <th>Done By</th>
	          <th>Approve By</th>
	          <th>Action</th>
	        </tr>
	      </thead>
	      <tbody>
					@unless (empty($calibration))
						@foreach ($calibration as $render)
							<tr>
								<td>{{ $i++ }}</td>
								<td>{{ $instrument->instrument_id }}</td>
								<td>{{ $render->customer }}</td>
								<td>{{ $render->done_by }}</td>
								<td>{{ $render->approve_by }}</td>
								<td>
								<div class="form-inline">
								  {!! Form::open(['method' => 'GET','url' => ['calibration/history', $render->id]]) !!}
								  	  {!! Form::hidden('instrument_id', $render->instrument_id) !!}
								  	  {!! Form::hidden('calibrated_at', $render->calibrated_at) !!}
								  	  {!! Form::submit('View Calibration', ['class' => 'btn btn-default']) !!}
								  {!! Form::close() !!}
								</div>
		          </td>
							</tr>
						@endforeach
					@endif
	      </tbody>
	    </table>
		</div>
	</div>
@stop

		{{-- 						  <a href="{{ 'calibration/test/create/' . $instrument->id }}" class="btn btn-default">Calibrate</a>
									<a href="{{ 'calibration/test/' . $instrument->id . '/edit' }}" class="btn btn-default">Edit Calibration</a> --}}