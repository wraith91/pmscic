@extends('layouts.master')

@section('title')
	Calibration
@stop

@section('header')
	@include ('layouts.secondary_menu', 
	[
		'title' => 'Calibration', 
		'secondary_menu' => 
			[
				'Back' => 'calibration'
			],
	])
@stop



@section('content')
	<div class="inner-content content-block">
	  	<div class="file-title">
	  		<h1>Power ReadJj, INC.</h1>
	  		<h3>Electrical Insturmentation &amp Controls-Engineering</h3>
	  	</div>
		  <table class="preview-table">
			  <tbody>
			  	@foreach ($calibration as $item)
			  	<tr>
		  			<td><strong>Customer</strong></td>
		  			<td>:</td>
		  			<td><p>{{ $item->customer }}</p></td>
		  			<td><strong>Calibration Date</strong></td>
		  			<td>:</td>
		  			@foreach ($dates as $date)
		  				<td><p>{{ $date->calibrated_at->format('F, d, Y') }}</p></td>
		  			@endforeach
		  		</tr>
		  		<tr>
		  			<td><strong>Address</strong></td>
		  			<td>:</td>
		  			<td><p>{{ $item->address }}</p></td>
		  			<td><strong>Recall Date</strong></td>
		  			<td>:</td>
		  			@foreach ($dates as $date)
		  				<td><p>{{ $date->recalled_at->format('F, d, Y') }}</p></td>
		  			@endforeach
		  		</tr>
					<tr>
		  			<td colspan="3"></td>
		  			<td><strong>Location</strong></td>
		  			<td>:</td>
		  			<td><p>{{ $item->instrument->location->name }}</p></td>
		  		</tr>
		  		<tr>
		  			<td><strong>I.D #</strong></td>
		  			<td>:</td>
		  			<td><p>{{ $item->instrument->instrument_id }}</p></td>
		  			<td><strong>Application</strong></td>
		  			<td>:</td>
		  			<td><p>{{ $item->instrument->application }}</p></td>
		  		</tr>
		  		<tr>
		  			<td><strong>Function</strong></td>
		  			<td>:</td>
		  			<td><p>{{ $item->instrument->description }}</p></td>
		  			<td><strong>Input</strong></td>
		  			<td>:</td>
		  			<td><p>{{ $item->input }}</p></td>
		  		</tr>
		  		<tr>
		  			<td><strong>Brand</strong></td>
		  			<td>:</td>
		  			<td><p>{{ $item->instrument->trade_name }}</p></td>
		  			<td><strong>Output</strong></td>
		  			<td>:</td>
		  			<td><p>{{ $item->output }}</p></td>
		  		</tr>
		  		<tr>
		  			<td><strong>Model/Type</strong></td>
		  			<td>:</td>
		  			<td><p>{{ $item->model }}</p></td>
		  			<td><strong>Tag</strong></td>
		  			<td>:</td>
		  			<td><p>{{ $item->tag }}</p></td>
		  		</tr>
		  		<tr>
		  			<td><strong>Serial No.</strong></td>
		  			<td>:</td>
		  			<td><p>{{ $item->serial_no }}</p></td>
		  			<td><strong>Ref I.D</strong></td>
		  			<td>:</td>
		  			<td><p>{{ $item->ref_id }}</p></td>
		  		</tr>
		  		<tr>
		  			<td><strong>Other Specs</strong></td>
		  			<td>:</td>
		  			<td><p>{{ $item->other_specs }}</p></td>
		  			<td colspan="3"></td>
		  		</tr>
			  		<tr>
			  			<td class="title-block" colspan="6">Calibration Data</td>
			  		</tr>
			  </tbody>
		  </table>
		  <table class="calibration-table">
		  	<thead>
		  		<tr>
		  			<th>Scale</th>
		  			<th>Input</th>
		  			<th>Output</th>
		  			<th>Calibrated Output</th>
		  			<th>Error</th>
		  		</tr>
		  	</thead>
		  	<tbody>
		  		@foreach ($tests as $test)
						<tr>
		      		<td>{{ $test->scale }}</td>
		      		<td>{{ $test->input }}</td>
		      		<td>{{ $test->output }}</td>
		      		<td>{{ $test->after_calibration }}</td>
		      		<td>{{ $test->error }}</td>
		      	</tr>
	      	@endforeach
		  	</tbody>
		  </table>
		  <table class="preview-table">
		  		<tr>
		  			<td colspan="6"><strong>Description of Work Done:</strong></td>
		  		</tr>
		  		<tr>
		  			<td colspan="6"><p>{{ $item->remarks }}</p></td>
		  		</tr>
		  		<tr>
		  			<td><strong>Done By</strong></td>
		  			<td>:</td>
		  			<td><p>{{ $item->done_by }}</p></td>
		  			<td colspan="3"></td>
		  		</tr>
		  		<tr>
		  			<td><strong>Approve By</strong></td>
		  			<td>:</td>
		  			<td><p>{{ $item->approve_by }}</p></td>
		  			<td colspan="3"></td>
		  		</tr>
		  </table>
		  @endforeach
	</div>
@stop