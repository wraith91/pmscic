@extends('layouts.master')

@section('title')
	System
@stop

@section('header')
	@include ('layouts.secondary_menu', 
	[
		'title' => 'System', 
		'secondary_menu' => 
			[
				'Location' => 'system/location',
				'User' => 'system/user',
				'Certificate' => 'system/certificate'
			],
	])
@stop
