@extends('layouts.master')

@section('title')
	System/User
@stop

@section('header')
	@include ('layouts.secondary_menu', 
	[
		'title' => 'System', 
		'secondary_menu' => 
			[
				'Back' => 'system/certificate'
			],
	])
@stop

@section('content')
	<div class="inner-content content-block">
		  <div class="form">
		  	{!! Form::open(['url' => 'system/certificate']) !!}
		  	<div class="form-inline">
		  		  {!! Form::label('cert_id', 'Cert No:') !!}
		  		  {!! Form::text('cert_id', null, ['class' => 'form-control']) !!}
		  	</div>
		  	<div class="form-inline">
		  		  {!! Form::label('type', 'Type:') !!}
		  		  {!! Form::text('type', null, ['class' => 'form-control']) !!}
		  	</div>
		  	<div class="form-inline">
		  		{!! Form::label('', '') !!}
		  		{!! Form::submit('Create', ['class' => 'btn btn-primary form-control']) !!}
		  	</div>
		  {!! Form::close() !!}
		  </div>
	</div>
@stop