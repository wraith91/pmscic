@extends('layouts.master')

@section('title')
	System/User
@stop

@section('header')
	@include ('layouts.secondary_menu', 
	[
		'title' => 'System', 
		'secondary_menu' => 
			[
				'Back' => 'system/certificate'
			],
	])
@stop

@section('content')
	<div class="inner-content content-block">
		  <div class="form">
		  	{!! Form::model($certificate, ['method' => 'PUT', 'url' => ['system/certificate', $certificate->id]]) !!}
		  	<div class="form-inline">
		  		  {!! Form::label('cert_id', 'Cert No:') !!}
		  		  {!! Form::text('cert_id', null, ['class' => 'form-control']) !!}
		  	</div>
		  	<div class="form-inline">
		  		  {!! Form::label('type', 'Type:') !!}
		  		  {!! Form::text('type', null, ['class' => 'form-control']) !!}
		  	</div>
		  	<div class="form-inline">
		  		{!! Form::label('', '') !!}
		  		{!! Form::submit('Modify', ['class' => 'btn btn-warning form-control']) !!}
		  	</div>
		  {!! Form::close() !!}
		  </div>
	</div>
@stop