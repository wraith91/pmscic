@extends('layouts.master')

@section('title')
	System/User
@stop

@section('header')
	@include ('layouts.secondary_menu', 
	[
		'title' => 'System', 
		'secondary_menu' => 
			[
				'Add New Certificate' => 'system/certificate/create'
			],
	])
@stop

@section('content')
	<div class="inner-content content-block">
		<div class="panel panel-default">
	  <!-- Default panel contents -->
		  <div class="panel-heading">Certificate List</div>

		  <!-- Table -->
			<table class="table table-hover">
	      <thead>
	        <tr>
	          <th>#</th>
	          <th>Certificate ID</th>
	          <th>Created At</th>
	          <th>Action</th>
	        </tr>
	      </thead>
	      <tbody>
					@unless (empty($certifications))
						@foreach ($certifications as $certification)
							<tr>
								<td>{{ $i++ }}</td>
								<td>{{ $certification->cert_id}}</td>
								<td>{{ $certification->type}}</td>
								<td>{{ $certification->created_at->format('Y-m-d')}}</td>
								<td>
								  {!! Form::open(['method' => 'DELETE', 'url' => ['system/certificate', $certification->id]]) !!}
								  	{!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
								  	<a href="certificate/{{ $certification->id }}/edit" class="btn btn-warning">Edit</a>
								  {!! Form::close() !!}
								</td>
							</tr>
						@endforeach
					@endif
	      </tbody>
	    </table>
		</div>
	</div>
@stop
