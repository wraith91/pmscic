@extends('layouts.master')

@section('title')
	System/User
@stop

@section('header')
	@include ('layouts.secondary_menu', 
	[
		'title' => 'System', 
		'secondary_menu' => 
			[
				'Add New User' => 'system/user/create'
			],
	])
@stop

@section('content')
	<div class="inner-content content-block">
		<div class="panel panel-default">
	  <!-- Default panel contents -->
		  <div class="panel-heading">User List</div>

		  <!-- Table -->
			<table class="table table-hover">
	      <thead>
	        <tr>
	          <th>#</th>
	          <th>Name</th>
	          <th>Email</th>
	          <th>Action</th>
	        </tr>
	      </thead>
	      <tbody>
					@unless (empty($users))
						@foreach ($users as $user)
							<tr>
								<td>{{ $i++ }}</td>
								<td>{{ $user->first_name . " " . $user->last_name}}</td>
								<td>{{ $user->email }}</td>
								<td>
								  {!! Form::open(['method' => 'DELETE', 'url' => ['system/user', $user->id]]) !!}
								  	{!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
								  	<a href="user/{{ $user->id }}/edit" class="btn btn-warning">Edit</a>
								  {!! Form::close() !!}
								</td>
							</tr>
						@endforeach
					@endif
	      </tbody>
	    </table>
		</div>
	</div>
@stop
