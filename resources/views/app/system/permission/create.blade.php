@extends('layouts.master')

@section('title')
	System/User
@stop

@section('header')
	@include ('layouts.secondary_menu', 
	[
		'title' => 'System', 
		'secondary_menu' => 
			[
				'Back' => 'system/user'
			],
	])
@stop

@section('content')
	<div class="inner-content content-block">
		  <div class="form">
		  	{!! Form::open(['url' => 'system/role']) !!}
		  	<div class="form-inline">
		  		  {!! Form::label('role', 'Role:') !!}
		  		  {!! Form::select('role', $role, null, ['class' => 'form-control']) !!}
		  	</div>
		  	<div class="form-inline">
		  		  {!! Form::label('permission', 'Permissions:') !!}
		  		  {!! Form::select('permission', $permission, null, ['class' => 'form-control']) !!}
		  	</div>
		  	<div class="form-inline">
		  		{!! Form::label('', '') !!}
		  		{!! Form::submit('Save', ['class' => 'btn btn-primary form-control']) !!}
		  	</div>
		  {!! Form::close() !!}
		  </div>
	</div>
@stop