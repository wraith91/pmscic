@extends('layouts.master')

@section('title')
	System/Location
@stop

@section('header')
	@include ('layouts.secondary_menu', 
	[
		'title' => 'System', 
		'secondary_menu' => 
			[
				'Add New Location' => 'system/location/create'
			],
	])
@stop

@section('content')
	<div class="inner-content content-block">
		<div class="panel panel-default">
	  <!-- Default panel contents -->
		  <div class="panel-heading">Calibration List</div>

		  <!-- Table -->
			<table class="table table-hover">
	      <thead>
	        <tr>
	          <th>#</th>
	          <th>Prefix</th>
	          <th>Name</th>
	          <th>Action</th>
	        </tr>
	      </thead>
	      <tbody>
					@unless (empty($locations))
						@foreach ($locations as $location)
							<tr>
								<td>{{ $i++ }}</td>
								<td>{{ $location->code }}</td>
								<td>{{ $location->name }}</td>
								<td>
									  {!! Form::open(['method' => 'DELETE', 'url' => ['system/location', $location->id]]) !!}
									  	{!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
									  {!! Form::close() !!}

								</td>
							</tr>
						@endforeach
					@endif
	      </tbody>
	    </table>
		</div>
	</div>
@stop

		{{-- 						  <a href="{{ 'calibration/test/create/' . $instrument->id }}" class="btn btn-default">Calibrate</a>
									<a href="{{ 'calibration/test/' . $instrument->id . '/edit' }}" class="btn btn-default">Edit Calibration</a> --}}