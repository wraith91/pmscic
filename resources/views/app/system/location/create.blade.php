@extends('layouts.master')

@section('title')
	System/Location
@stop

@section('header')
	@include ('layouts.secondary_menu', 
	[
		'title' => 'System', 
		'secondary_menu' => 
			[
				'Back' => 'system/location'
			],
	])
@stop

@section('content')
	<div class="inner-content content-block">
		  <div class="form">
		  	{!! Form::open(['url' => 'system/location']) !!}
		  	<div class="form-inline">
		  		  {!! Form::label('code', 'Location Prefix:') !!}
		  		  {!! Form::text('code', null, ['class' => 'form-control', 'placeholder' => 'The prefix in the instrument id']) !!}
		  	</div>
		  	<div class="form-inline">
		  		  {!! Form::label('name', 'Location Name') !!}
		  		  {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Location Name - Prefix']) !!}
		  	</div>
		  	<div class="form-inline">
		  		{!! Form::label('', '') !!}
		  		{!! Form::submit('Save', ['class' => 'btn btn-primary form-control']) !!}
		  	</div>
		  {!! Form::close() !!}
		  </div>
	</div>
@stop