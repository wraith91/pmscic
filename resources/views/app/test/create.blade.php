	@extends('layouts.master')

@section('title')
	Instrument
@stop

@section('header')
	@include ('layouts.secondary_menu', 
	[
		'title' => 'Instrument', 
		'secondary_menu' => 
			[
				'Back' => 'instrument'
			],
	])
@stop

@section('content')
	<div class="inner-content content-block">
		<div class="panel panel-default">
	  	<!-- Default panel contents -->
		  <div class="panel-heading">Instrument ID: <strong>{{$instrument->instrument_id}}</strong></div>
			<!-- Table -->
		  {!! Form::open(['url' => 'calibration/test', 'id' => 'dynamic']) !!}
				<table class="table table-hover">
		      <thead>
		        <tr>
		          <th>Scale</th>
		          <th>Desired Output (PSI)</th>
		          <th>Actual Output (PSI)</th>
		          <th>Calibrated Output (PSI)</th>
		          <th>Error</th>
		        </tr>
		      </thead>
		      <tbody id="itemlist">
				  	  {!! Form::hidden('instrument_id', $instrument->id) !!}
						  <tr>
								<td>{!! Form::text('scale[]', null, ['class' => 'form-control', 'required', 'data-cell' => 'A1']) !!}	</td>
								<td>{!! Form::text('input[]', null, ['class' => 'form-control', 'required', 'data-cell' => 'B1']) !!}</td>
								<td>{!! Form::text('output[]', null, ['class' => 'form-control', 'required', 'data-cell' => 'C1']) !!}</td>
								<td>{!! Form::text('after_calibration[]', null, ['class' => 'form-control', 'required', 'data-cell' => 'D1']) !!}</td>
								<td>{!! Form::text('error[]', null, ['class' => 'form-control', 'required', 'data-cell' => 'E1', 'data-format' => '0.0', 'data-formula' => 'C1 - B1 / C1 * 100']) !!}</td>
							</tr>
							<tr>
								<td>{!! Form::text('scale[]', null, ['class' => 'form-control', 'required', 'data-cell' => 'A2']) !!}	</td>
								<td>{!! Form::text('input[]', null, ['class' => 'form-control', 'required', 'data-cell' => 'B2']) !!}</td>
								<td>{!! Form::text('output[]', null, ['class' => 'form-control', 'required', 'data-cell' => 'C2']) !!}</td>
								<td>{!! Form::text('after_calibration[]', null, ['class' => 'form-control', 'required', 'data-cell' => 'D2']) !!}</td>
								<td>{!! Form::text('error[]', null, ['class' => 'form-control', 'required', 'data-cell' => 'E2', 'data-format' => '0.0', 'data-formula' => 'C2 - B2 / C2 * 100']) !!}</td>
							</tr>
							<tr>
								<td>{!! Form::text('scale[]', null, ['class' => 'form-control', 'required', 'data-cell' => 'A3']) !!}	</td>
								<td>{!! Form::text('input[]', null, ['class' => 'form-control', 'required', 'data-cell' => 'B3']) !!}</td>
								<td>{!! Form::text('output[]', null, ['class' => 'form-control', 'required', 'data-cell' => 'C3']) !!}</td>
								<td>{!! Form::text('after_calibration[]', null, ['class' => 'form-control', 'required', 'data-cell' => 'D3']) !!}</td>
								<td>{!! Form::text('error[]', null, ['class' => 'form-control', 'required', 'data-cell' => 'E3', 'data-format' => '0.0', 'data-formula' => 'C3 - B3 / C3 * 100']) !!}</td>
							</tr>
							<tr>
								<td>{!! Form::text('scale[]', null, ['class' => 'form-control', 'required', 'data-cell' => 'A4']) !!}	</td>
								<td>{!! Form::text('input[]', null, ['class' => 'form-control', 'required', 'data-cell' => 'B4']) !!}</td>
								<td>{!! Form::text('output[]', null, ['class' => 'form-control', 'required', 'data-cell' => 'C4']) !!}</td>
								<td>{!! Form::text('after_calibration[]', null, ['class' => 'form-control', 'required', 'data-cell' => 'D4']) !!}</td>
								<td>{!! Form::text('error[]', null, ['class' => 'form-control', 'required', 'data-cell' => 'E4', 'data-format' => '0.0', 'data-formula' => 'C4 - B4 / C4 * 100']) !!}</td>
							</tr>
							<tr>
								<td>{!! Form::text('scale[]', null, ['class' => 'form-control', 'required', 'data-cell' => 'A5']) !!}	</td>
								<td>{!! Form::text('input[]', null, ['class' => 'form-control', 'required', 'data-cell' => 'B5']) !!}</td>
								<td>{!! Form::text('output[]', null, ['class' => 'form-control', 'required', 'data-cell' => 'C5']) !!}</td>
								<td>{!! Form::text('after_calibration[]', null, ['class' => 'form-control', 'required', 'data-cell' => 'D5']) !!}</td>
								<td>{!! Form::text('error[]', null, ['class' => 'form-control', 'required', 'data-cell' => 'E5', 'data-format' => '0.0', 'data-formula' => 'C5 - B5 / C5 * 100']) !!}</td>
							</tr>

							<tr>
								<td>{!! Form::submit('Save', ['class' => 'btn btn-default']) !!}</td>
							</tr>
		      </tbody>
		    </table>
		  {!! Form::close() !!}
			</div>
		</div>
	</div>
@stop

@section('footer')
	<script>
        $('#dynamic').calx();
    </script>
@stop
