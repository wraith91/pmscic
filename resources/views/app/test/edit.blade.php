@extends('layouts.master')

@section('title')
	Instrument
@stop

@section('header')
	@include ('layouts.secondary_menu', 
	[
		'title' => 'Instrument', 
		'secondary_menu' => 
			[
				'Back' => 'calibration'
			],
	])
@stop

@section('content')
	<div class="inner-content content-block">
		<div class="panel panel-default">
	  	<!-- Default panel contents -->
		  <div class="panel-heading">Instrument ID: <strong>{{ $instrument->instrument_id }}</strong></div>
			<!-- Table -->
			<table class="table table-hover">
	      <thead>
	        <tr>
	          <th>Scale</th>
	          <th>Input</th>
	          <th>Output</th>
	          <th>After Calibration</th>
	          <th>Error</th>
	        </tr>
	      </thead>
	      <tbody>
				  {!! Form::model($instrument, ['method' => 'PATCH', 'url' => ['calibration/test', $instrument->id]]) !!}
			  	  {!! Form::hidden('instrument_id', $instrument->id) !!}
						@foreach ($instrument->tests as $test)
						  <tr>
						  	{!! Form::hidden('test_id[]', $test->id) !!}
								<td>{!! Form::text('scale[]', $test->scale, ['class' => 'form-control']) !!}</td>
								<td>{!! Form::text('input[]', $test->input, ['class' => 'form-control']) !!}</td>
								<td>{!! Form::text('output[]', $test->output, ['class' => 'form-control']) !!}</td>
								<td>{!! Form::text('after_calibration[]', $test->after_calibration, ['class' => 'form-control']) !!}</td>
								<td>{!! Form::text('error[]', $test->error, ['class' => 'form-control']) !!}</td>
							</tr>
						@endforeach
						<tr>
							<td>{!! Form::submit('Modify', ['class' => 'btn btn-default']) !!}</td>
						</tr>
				  {!! Form::close() !!}
	      </tbody>
	    </table>
			</div>
		</div>
	</div>
@stop