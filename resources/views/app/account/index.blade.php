@extends('layouts.master')

@section('title')
	Account Settings
@stop

@section('header')
	@include ('layouts.secondary_menu', 
	[
		'title' => 'Account Settings', 
	])
@stop

@section('content')
	<div class="inner-content content-block">
		 <div class="form">
		  	{!! Form::model($user, ['method' => 'GET', 'url' => ['account/settings/' . $user->id . '/edit']]) !!}
		  	<div class="form-inline">
		  		    {!! Form::label('email', 'Email') !!}
		  		    {!! Form::text('email', null, ['class' => 'form-control', 'disabled']) !!}
		  	</div>
		  	<div class="form-inline">
			  		  {!! Form::label('first_name', 'First Name:') !!}
			  		  {!! Form::text('first_name', null, ['class' => 'form-control', 'disabled']) !!}
		  	</div>
		  	<div class="form-inline">
			  		  {!! Form::label('last_name', 'Last Name:') !!}
			  		  {!! Form::text('last_name', null, ['class' => 'form-control', 'disabled']) !!}
		  	</div>
		  	<div class="form-inline">
		  		{!! Form::label('', '') !!}
		  		{!! Form::submit('Edit', ['class' => 'btn btn-warning form-control']) !!}
		  	</div>
		  {!! Form::close() !!}
	  </div>
	</div>
@stop