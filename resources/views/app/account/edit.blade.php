@extends('layouts.master')

@section('title')
	Account Settings
@stop

@section('header')
	@include ('layouts.secondary_menu', 
	[
		'title' => 'Account Settings', 
		'secondary_menu' => 
			[
				'Back' => 'account/settings'
			],
	])
@stop

@section('content')
	<div class="inner-content content-block">
		 <div class="form">
		  	{!! Form::model($user, ['method' => 'PATCH', 'url' => ['account/settings/' . $user->id]]) !!}
		  	<div class="form-inline">
		  		    {!! Form::label('email', 'Email:') !!}
		  		    {!! Form::text('email', null, ['class' => 'form-control']) !!}
		  	</div>
		  	<div class="form-inline">
			  		  {!! Form::label('first_name', 'First Name:') !!}
			  		  {!! Form::text('first_name', null, ['class' => 'form-control']) !!}
		  	</div>
		  	<div class="form-inline">
			  		  {!! Form::label('last_name', 'Last Name:') !!}
			  		  {!! Form::text('last_name', null, ['class' => 'form-control']) !!}
		  	</div>
		  	<div class="form-inline">
			  		  {!! Form::label('password', 'Password:') !!}
			  		  {!! Form::password('password', ['class' => 'form-control']) !!}
		  	</div>
		  	<div class="form-inline">
			  		  {!! Form::label('password_confirmation', 'Confirm Password:') !!}
			  		  {!! Form::Password('password_confirmation', ['class' => 'form-control']) !!}
		  	</div>
		  	<div class="form-inline">
		  		{!! Form::label('', '') !!}
		  		{!! Form::submit('Update', ['class' => 'btn btn-primary form-control']) !!}
		  	</div>
		  {!! Form::close() !!}
	  </div>
	</div>
@stop