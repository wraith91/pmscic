@extends('layouts.master')

@section('title')
	Account Registration
@stop

@section('header')
	<h1>User Registration</h1>
@stop

@section('content')
	<div class="inner-content">
		  {!! Form::open(['action' => 'RegistrationController@store']) !!}
		  	<div class="form-group">
		  	  {!! Form::label('first_name', 'First Name') !!}
		  	  {!! Form::text('first_name', null, ['class' => 'form-control']) !!}
		  	</div>
		  	<div class="form-group">
	  		  {!! Form::label('last_name', 'Last Name') !!}
	  		  {!! Form::text('last_name', null, ['class' => 'form-control']) !!}
		  	</div>
		  	<div class="form-group">
	  		  {!! Form::label('email', 'Email') !!}
	  		  {!! Form::text('email', null, ['class' => 'form-control']) !!}
		  	</div>
		  	<div class="form-group">
		  	  {!! Form::label('password', 'Password') !!}
		  	  {!! Form::password('password', ['class' => 'form-control']) !!}
		  	</div>
		  	<div class="form-group">
		  	  {!! Form::label('password_confirmation', 'Password Confirmation') !!}
		  	  {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
		  	</div>
		  	<div class="form-group">
		  	{!! Form::submit('Register', ['class' => 'btn btn-primary']) !!}
		  	</div>
		  {!! Form::close() !!}
	</div>
@stop