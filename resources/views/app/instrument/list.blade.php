@extends('layouts.master')

@section('title')
	Instrument
@stop

@section('header')
	@include ('layouts.secondary_menu', 
	[
		'title' => 'Instrument', 
		'secondary_menu' => 
			[
				'Add New Instrument' => 'instrument/create',
				'Back' => 'instrument',
			],
	])
@stop

@section('content')
	<div class="inner-content content-block">
		<div class="panel panel-default">
	  <!-- Default panel contents -->
		  <div class="panel-heading">Instrument List</div>
		  <!-- Table -->
			<table class="table table-hover">
	      <thead>
	        <tr>
	          <th>#</th>
	          <th>ID</th>
	          <th>Description</th>
	          <th>Action</th>
	        </tr>
	      </thead>
	      <tbody>
			  	@foreach ($instruments as $instrument)
						<tr>
		          <th scope="row">1</th>
		          <td><a href="{{ 'instrument/' . $instrument->id }}">{{ $instrument->instrument_id }}</a></td>
		          <td>{{ $instrument->description }}</td>
		          <td>
								<div class="form-inline">
								  {!! Form::open(['method' => 'DELETE', 'url' => ['instrument', $instrument->id]]) !!}
								  	@can('edit_instruments')
								  	<a href="{{ 'instrument/' . $instrument->id . '/edit' }}" class="btn btn-default">Edit</a>
								  	@endcan
								  	<a href="{{ 'instrument/' . $instrument->id . '/history' }}" class="btn btn-default">History Dates</a>
								  	@can('delete_instruments')
								  	{!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
								  	@endcan
								  {!! Form::close() !!}
								</div>
		          </td>
		        </tr>
	        @endforeach
	      </tbody>
	    </table>
		</div>
	</div>
@stop