@extends('layouts.master')

@section('title')
	Instrument
@stop

@section('header')
	@include ('layouts.secondary_menu', 
	[
		'title' => 'Instrument', 
		'secondary_menu' => 
			[
				'Back' => 'instrument'
			],
	])
@stop


@section('content')
	<div class="inner-content content-block">
		<div class="panel panel-default">
		  <!-- Default panel contents -->
		  <div class="panel-heading">Instrument Record ID: <strong>{{ $instrument->instrument_id }}</strong></div>

		  <!-- Table -->
		  <table class="table">
		    <tbody>
		    	<tr>
		    		<td><strong>Instrument ID:</strong></td>
		    		<td>{{ $instrument->instrument_id }}</td>
		    	</tr>
		    	<tr>
		    		<td><strong>Description:</strong></td>
		    		<td>{{ $instrument->description }}</td>
		    	</tr>
		    	<tr>
		    		<td><strong>Trade Name:</strong></td>
		    		<td>{{ $instrument->trade_name }}</td>
		    	</tr>
		    	<tr>
		    		<td><strong>Specification:</strong></td>
		    		<td>{{ $instrument->specification }}</td>
		    	</tr>
		    	<tr>
		    		<td><strong>Application:</strong></td>
		    		<td>{{ $instrument->application }}</td>
		    	</tr>
		    	<tr>
		    		<td><strong>Calibration Date:</strong></td>
		    		<td>{{ $testDate->calibrated_at->format('Y-m-d') }}</td>
		    	</tr>
		    	<tr>
		    		<td><strong>Recall Date:</strong></td>
		    		<td>{{ $testDate->recalled_at->format('Y-m-d') }}</td>
		    	</tr>	
		    </tbody>
		  </table>
		</div>
	</div>
@stop