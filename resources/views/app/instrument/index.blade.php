@extends('layouts.master')

@section('title')
	Instrument
@stop

@section('header')
	@include ('layouts.secondary_menu', 
	[
		'title' => 'Instrument', 
		'secondary_menu' => 
			[
				'Add New Instrument' => 'instrument/create',
				'List' => 'instrument/lists',
			],
	])
@stop

@section('content')
	<div class="search-content content-block">
		  {!! Form::open(['method' => 'GET' , 'url' => 'instrument' ,'class' => 'navbar-form']) !!}
		  	<div class="form-group">
		  		{!! Form::label('search', 'Search Instrument ID:') !!}
		  	  {!! Form::text('search', null, ['class' => 'form-control', 'required']) !!}
		  	</div>
		  		{!! Form::submit('Search', ['class' => 'btn btn-default']) !!}
		  {!! Form::close() !!}
	</div>
	<div class="inner-content content-block">
		<div class="panel panel-default">
	  <!-- Default panel contents -->
		  <div class="panel-heading">Instrument List</div>

		  <!-- Table -->
			<table class="table table-hover">
	      <thead>
	        <tr>
	          <th>#</th>
	          <th>ID</th>
	          <th>Description</th>
	          <th>Calibration Date</th>
	          <th>Recall Date</th>
	          <th>Action</th>
	        </tr>
	      </thead>
	      <tbody>
				  @unless (empty($instrument))
						<tr>
		          <th scope="row">1</th>
		          <td><a href="{{ 'instrument/' . $instrument->id }}">{{ $instrument->instrument_id }}</a></td>
		          <td>{{ $instrument->description }}</td>
		          <td>{{ $testDate->calibrated_at->format('Y-m-d') }}</td>
		          <td>{{ $testDate->recalled_at->format('Y-m-d') }}</td>
		          <td>
								<div class="form-inline">
								  {!! Form::open(['method' => 'DELETE', 'url' => ['instrument', $instrument->id]]) !!}
								  	@can('edit_instruments')
								  	<a href="{{ 'instrument/' . $instrument->id . '/edit' }}" class="btn btn-default">Edit</a>
								  	@endcan
								  	<a href="{{ 'instrument/' . $instrument->id . '/history' }}" class="btn btn-default">History Dates</a>
								  	@can('delete_instruments')
								  	{!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
								  	@endcan
								  {!! Form::close() !!}
								</div>
		          </td>
		        </tr>
				  @endif
	      </tbody>
	    </table>
		</div>
	</div>
@stop