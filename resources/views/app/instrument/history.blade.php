@extends('layouts.master')

@section('title')
	Instrument
@stop

@section('header')
	@include ('layouts.secondary_menu', 
	[
		'title' => 'Instrument', 
		'secondary_menu' => 
			[
				'Back' => 'instrument'
			],
	])
@stop


@section('content')
	<div class="inner-content content-block">
		<div class="panel panel-default">
		  <!-- Default panel contents -->
		  <div class="panel-heading">History Dates for Instrument ID: <strong>{{ $instrument->instrument_id }}</strong></div>

		  <!-- Table -->
		  <table class="table">
		    <thead>
		    	<th>No.</th>
		    	<th>Calibration</th>
		    	<th>Recall</th>
		    </thead>
		    <tbody>
		    	@foreach ($dates as $date)
						<tr>
							<td>{{ $i++ }}</td>
			    		<td>{{ $date->calibrated_at->format('Y-m-d') }}</td>
			    		<td>{{ $date->recalled_at->format('Y-m-d') }}</td>
		    		</tr>
		    	@endforeach
		    </tbody>
		  </table>
		</div>
	</div>
@stop