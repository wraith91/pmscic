@extends('layouts.master')

@section('title')
	Instrument
@stop

@section('header')
	@include ('layouts.secondary_menu', 
	[
		'title' => 'Instrument', 
		'secondary_menu' => 
			[
				'Back' => 'instrument'
			],
	])
@stop

@section('content')
	<div class="inner-content content-block">
	  <div class="form">
	  	{!! Form::open(['url' => 'instrument']) !!}
		  	<div class="form-inline">
		  		  {!! Form::label('location_id', 'Location:') !!}
		  		  {!! Form::select('location_id', $location, ['class' => 'form-control']) !!}
		  	</div>
		  	<div class="form-inline">
		  		  {!! Form::label('instrument_id', 'Instrument ID') !!}
		  		  {!! Form::text('instrument_id', null, ['class' => 'form-control', 'placeholder' => 'Instrument designated ID/Tag']) !!}
		  	</div>
		  	<div class="form-inline">
		  		  {!! Form::label('description', 'Description:') !!}
		  		  {!! Form::text('description', null, ['class' => 'form-control']) !!}
		  	</div>
		  	<div class="form-inline">
		  		  {!! Form::label('application', 'Application:') !!}
		  		  {!! Form::text('application', null, ['class' => 'form-control']) !!}
		  	</div>
		  	<div class="form-inline">
		  		  {!! Form::label('trade_name', 'Trade Name') !!}
		  		  {!! Form::text('trade_name', null, ['class' => 'form-control']) !!}
		  	</div>
		  	<div class="form-inline">
		  		  {!! Form::label('specification', 'Specification') !!}
		  		  {!! Form::text('specification', null, ['class' => 'form-control']) !!}
		  	</div>
		  	<div class="form-inline">
		  		  {!! Form::label('calibrated_at', 'Calibrated Date:') !!}
		  		  {!! Form::input('date', 'calibrated_at', date('Y-m-d'), ['class' => 'form-control']) !!}
		  	</div>
		  	<div class="form-inline">
		  		  {!! Form::label('recalled_at', 'Recalled Date:') !!}
		  		  {!! Form::input('date', 'recalled_at', date('Y-m-d'), ['class' => 'form-control']) !!}
		  	</div>
		  	<div class="form-inline">
		  		{!! Form::label('', '') !!}
		  		{!! Form::submit('Save', ['class' => 'btn btn-primary form-control']) !!}
		  	</div>
		  {!! Form::close() !!}
	  </div>
	</div>
@stop