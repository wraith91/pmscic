  @if (Session::has('flash_message'))
    <section class="alert alert-success">
        <p>{!! Session::get('flash_message') !!}</p>
    </section>
  @endif