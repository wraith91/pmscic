<aside>
  <div class="logo">
    <h1>PMSCIC</h1>
  </div>
  <nav>
    <ul class="emp-menu">
      @can ('view_dashboard')
      <li class="{{ set_active(['dashboard']) }}">
        <a href="{!! URL::to('dashboard') !!}">
          <img class="iconic" data-src="{{ asset('main/images/home.svg') }}" />
          Dashboard
        </a>
      </li>
      @endcan
      <li class="{{ set_active(['instrument', 'instrument/*']) }}">
        <a href="{!! URL::to('instrument') !!}">
          <img class="iconic" data-src="{{ asset('main/images/dashboard.svg') }}" />
          <span>Instrument</span>
        </a>
      </li>
      @can ('view_calibrations')
      <li class="{{ set_active(['calibration', 'calibration/*']) }}">
        <a href="{!! URL::to('calibration') !!}">
          <img class="iconic" data-src="{{ asset('main/images/list.svg') }}" />
          <span>Calibration</span>
        </a>
      </li>
      @endcan
      <li class="{{ set_active(['account', 'account/*']) }}">
        <a href="{!! URL::to('account/settings') !!}">
          <img class="iconic" data-src="{{ asset('main/images/settings.svg') }}" />
          <span>Account</span>
        </a>
      </li>
      @can ('edit_system_settings')
        <li class="{{ set_active(['system', 'system/*']) }}">
          <a href="{!! URL::to('system') !!}">
            <img class="iconic" data-src="{{ asset('main/images/cogs.svg') }}" />
            <span>System</span>
          </a>
        </li>
      @endcan
      <li>
        <a href="{!! URL::to('auth/logout') !!}">
          <img class="iconic" data-src="{{ asset('main/images/account.svg') }}" />
          <span>Logout</span>
        </a>
      </li>
    </ul>
  </nav>
  @can('create_user')
    <ul class="admin-menu">
      <li><a href="account/register">User Accounts</a></li>
    </ul>
  @endcan

</aside>