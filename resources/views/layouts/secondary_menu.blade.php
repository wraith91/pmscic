  <h1 class="header-greeting">{{ $title }}</h1>
  @if (isset($secondary_menu) && count($secondary_menu) > 0)
    <ul>
	    @foreach ($secondary_menu as $menu_item => $link)
	    	<li><a href="{{ URL::to($link ) }}" class="btn btn-default">{{ ucfirst($menu_item)  }}</a></li>
	    @endforeach
{{-- 	    <li>{!! Form::text('search', null, ['class' => 'form-control', 'placeholder' => 'Search Records']) !!}</li> --}}
    </ul>
  @endif

  