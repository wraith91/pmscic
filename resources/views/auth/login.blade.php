@extends('layouts.login')

@section('form')
	{!! Form::open(['url' => 'auth/login']) !!}
  	<div class="form-group">
      	{!! Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'Email']) !!}
  	</div>
  	<div class="form-group">
  		  {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Password']) !!}
  	</div>
  	<div class="checkbox">
	    <label>
	      <input type="checkbox"> Remember me
	    </label>
	  </div>
  	<div class="form-group">
  			{!! Form::submit('Login', ['class' => 'btn btn-primary']) !!}
  	</div>
  	<div class="form-inline">
  			<a id="links" href="register" class="btn btn-link">Forgot Password?</a>
  	</div>
  {!! Form::close() !!}
@stop