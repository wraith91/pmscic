var elixir = require('laravel-elixir');
require('laravel-elixir-sass-compass');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.compass('app.scss', 'public/main/css/', {
    	sass: "resources/assets/sass",
    	font: "public/main/fonts",
		  image: "public/main/images",
		  javascript: "public/main/js",
    	sourcemap: false
    })
    .copy(
	  	'resources/assets/fonts',
	  	'public/main/fonts'
  	)
    .copy(
	  	'resources/assets/images',
	  	'public/main/images'
  	)
    .copy(
        'resources/assets/js/iconic.min.js',
        'public/main/js'
    )
    .copy(
            'resources/assets/components/jquery-calx/js/numeral.min.js', 
            'public/main/js'
    )
    .copy(
            'resources/assets/components/jquery-calx/jquery-calx-2.2.5.min.js', 
            'public/main/js'
    )    
    .copy(
            'resources/assets/components/jquery-calx/jquery-1.9.1.min.js', 
            'public/main/js'
    )
    .scripts([
        'jquery-1.9.1.min.js',
        'iconic.min.js',
        'numeral.min.js',
        'jquery-calx-2.2.5.min.js'
    ], 'public/main/js', 'public/main/js');
});
